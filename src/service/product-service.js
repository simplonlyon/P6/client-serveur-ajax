import $ from 'jquery';
import { Product } from '../entity/product';

/**
 * Cette classe permettra de regrouper toutes les méthodes d'accès
 * à l'API REST via des requêtes AJAX.
 * L'idée est de ne pas avoir des appels AJAX brut qui traînent dans
 * l'appli et de n'utiliser que cette classe pour ça (elle fera
 * le CRUD en HTTP en gros)
 */
export class ProductService {


    findAll() {
        /**
         * On fait un return de $.ajax pour passer la Promise lorsqu'on
         * appelle cette méthode (il faudra donc faire un .then() 
         * quand on l'appelera)
         */
        return $.ajax('http://localhost:2999/products')
        /* On met un .then() ici pour transformer les données de
        la promesses afin que lorsqu'on appelle cette méthode on
        manipule directement une Promise de Product[] plutôt que
        une Promise d'Object ressemblant vaguement à un Product */
        .then(function(data) {
            let tab = [];
            //On boucle sur les données brutes
            for (const item of data) {
                //On crée une instance de Product pour chaque objet
                let product = new Product(item.name, item.description, item.price, item.id);
                tab.push(product);
            }
            //on return le tableau de Product pour le .then() suivant
            return tab;
            
        });
    }


}
