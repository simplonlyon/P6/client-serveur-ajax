
function asyncAdd(a, b) {
    /**
     * On peut créer des Promise nous même via la classe native JS
     * Celle ci attend en paramètre de son constructeur une fonction
     * avec 2 paramètre, le premier (resolve) représente la fonction
     * qui sera donnée dans le then, le second (reject) représente
     * la fonction qu'on donnera dans le catch.
     * On indique ensuite dans la promise dans quel cas on resolve
     * (promise qui fonctionne) et dans quel cas on reject (promise
     * qui a un soucis).
     * Les paramètres qu'on donne dans le resolve et dans le reject
     * seront accessibles dans la function du then ou du catch
     */
    let promise = new Promise(function(resolve, reject) {
        if(typeof(a) !== 'number' || typeof(b) !== 'number') {
            reject("parameters must be numbers");
        }

        setTimeout(function() {
            resolve(a+b);
        }, Math.random()*3000);
        
    });

    return promise;

}

asyncAdd(2,3).then(function(data) {
    console.log(data);
}).catch(function(err) {
    console.log(err);
});

//On peut utiliser le Promise.all pour faire qu'on exécute le then
//uniquement lorsque les 2 Promise ont été résolues sans problème
Promise.all([asyncAdd(2,3), asyncAdd(1,1)])
.then(function(data) {
    console.log(data[0]*data[1]);
});